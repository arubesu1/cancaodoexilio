﻿namespace CancaoDoExilio
{
    public class Ave : Animal
    {
        public string TipoAsas
        {
            get;
            set;
        }

        public int gorjeio;
        private string nome;       

        public void AlterarNome(string nome)
        {
            this.nome = nome;
        }

        public void AlterarTipoAsas(string tipoAsas)
        {
            this.TipoAsas = tipoAsas;
        }

        public int ObterGojeio()
        {
            return 100 / gorjeio;
        }
    }
}