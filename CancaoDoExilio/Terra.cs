﻿using System.Collections.Generic;

namespace CancaoDoExilio
{
    public class Terra
    {
        public int QuantidadePalmeirasAvistadas = 7;      

        public string sabiaCanta = "True";

        public Palmeira[] palmeiras;

        public List<Ave> aves = new List<Ave>();

        private bool terminou; 

        public bool Terminou
        {
            get
            {
                return terminou;
            }
            set
            {
                terminou = value;
            }
        }

        public Terra()
        {
            this.palmeiras = new Palmeira[QuantidadePalmeirasAvistadas];
        }



        public double ObterQuantidadeCoisas()
        {
            double teste = 1.5;
            for (int i = 0; i < 20; i++)
            {
                teste = teste + 1.54;
            }

            return teste;
        }

        public bool ObterPrimores()
        {
            int x = 20;
            int y = 10;

            return x == 20 && y == 10;
        }
    }
}
