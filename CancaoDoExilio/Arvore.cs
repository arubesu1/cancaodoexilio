﻿using System;

namespace CancaoDoExilio
{
    public abstract class Arvore : Planta
    {
        public string Raiz;

        public abstract void Crescer();

        public virtual void Morrer()
        {
            Console.WriteLine("A Arvore Morreu");
        }

    }
}
