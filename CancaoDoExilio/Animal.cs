﻿using System;

namespace CancaoDoExilio
{
    public  class Animal
    {
        public void Andar()
        {
            Console.WriteLine("O animal andou.");
        }

        public void Comer()
        {
            Console.WriteLine("O animal comeu.");
        }
    }
}