﻿using System;
using CancaoDoExilio;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CancaoDoExilioTests
{
    [TestClass]
    public class CancaoDoExilioTest
    {
        Terra minhaTerra = new Terra();        

        [TestMethod]
        public void TestFirstStanza()
        {
            string stranza = "Minha Terra tem palmeiras,\nOnde canta o Sabiá;\nAs aves, que aqui gorjeiam,\nNão gorjeiam como lá.\n\n";
            Assert.AreEqual(stranza, CancaoExilio.GerarPrimeiraEstrofe(minhaTerra));
        }

        [TestMethod]
        public void TestSecondStanza()
        {
            string stranza = "Nosso céu têm mais estrelas,\nNossas várzeas têm mais flores,\nNossos bosques têm mais vida,\nNossa vida mais amores.\n\n";
            Assert.AreEqual(stranza, CancaoExilio.GerarSegundaEstrofe(minhaTerra));
        }

        [TestMethod]
        public void TestThirdStanza()
        {
            string stranza = " Em cismar, sozinho, à noite,\nMais prazer encontro eu lá;\nMinha terra tem palmeiras,\nOnde canta o Sabiá.\n\n";
            Assert.AreEqual(stranza, CancaoExilio.GerarTerceiraEstrofe(minhaTerra));
        }

        [TestMethod]
        public void TestFourthStanza()
        {
            string stranza = "Minha terra tem primores\nQue tais não encontro eu cá;\nEm cismar — sozinho, à noite —\nMais prazer encontro eu lá;\nMinha terra tem palmeiras,\nOnde canta o Sabiá.\n\n";
            Assert.AreEqual(stranza, CancaoExilio.GerarQuartaEstrofe(minhaTerra));
        }

        [TestMethod]
        public void TestFifthStanza()
        {
            string stranza = "Não permita Deus que eu morra,\nSem que eu volte para lá;\nSem que desfrute os primores\nQue não encontro por cá;\nSem qu'inda aviste as palmeiras,\nOnde canta o Sabiá.";
            Assert.AreEqual(stranza, CancaoExilio.GerarQuintaEstrofe(minhaTerra));
        }


    }
}
